1)Первым делом, убедитесь, что имеете представление о следующих понятиях:
- data class, их отличия от обычных классов
- чем val отличается от var, деклорирование полей в конструкторе
- структуры данных list и map, как обявить, наполнить, добавить элемент, достать по индексу/ключу

2)
читаем про либку jackson - она используется для того, чтобы конвертить обьекты в json формат и обратно
https://www.baeldung.com/jackson
https://www.baeldung.com/jackson-annotations
https://www.baeldung.com/jackson-object-mapper-tutorial
https://mkyong.com/java/how-to-convert-java-object-to-from-json-jackson/

3)
выбираем какой-нибудь httpclient:
rest-assured https://github.com/rest-assured/rest-assured/wiki/GettingStarted
okhttp  https://square.github.io/okhttp/4.x/okhttp/okhttp3/-ok-http-client/
любой другой

4)
гуглим как сделать get реквест и сконвертить json из String в обьект.


(если не гуглится, то вот , например
https://www.baeldung.com/okhttp-json-response
)


5) наконец, переходим к заданию


у openstreetmap.org есть некое API overpass:
(дока: https://wiki.openstreetmap.org/wiki/Overpass_API)

https://lz4.overpass-api.de/api/interpreter

В данном задании будем использовать сущность этого API - node

https://lz4.overpass-api.de/api/interpreter?data=[out:json];node(around:1000,55.752125,37.617665);out;
данный запрос возвращает список топографический отметок - описаний обьектов, расположенных в круге радиусом around:1000 с центром в географических координатах 55.752125,37.617665


обьекты можно фильтровать используя теги:
https://lz4.overpass-api.de/api/interpreter?data=[out:json];node[amenity=bar](around:1000,55.752125,37.617665);out;
-вернет список окрестных баров


Задача 1:
познакомится с моделью данный этого API
при помощи аннотаций jackson заимплементить класс-модель ( например в  org.example.GeoNodeResponseModel), соответствующего json респонсу

задача 2:

заимплементить метод, выполняющий get http response
( принимат аргументы
        altitude: Double, -широта
        longitude: Double, - долгота
        radius: Int, - радиус в метрах
        tag: String - тег
        и возвращает в качестве результата инстанс класска, заимплеменченого в задаче 1
)

задача 3:
написать тесты (sourceset - тест)
проверяющие следующее условие для трех точек:
проверить каждую точку на соответствие критерию: в радиусе 400 метров от точки есть хотя бы одна станция метро(фильтр - [station=subway]) и не менее двух баров(node[amenity=bar])

59.954039,30.4129438
59.9431492,30.2772645,
59.9390267,30.3442583
