package org.example

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class GeoNodeResponseModel(
    val version: Double,
    val generator: String,
    val osm3s: Map<String, Any>,
    val elements: List<ElementResponseModel>,
)
