package org.example

import okhttp3.OkHttpClient
import okhttp3.Request
import okio.IOException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

object OverpassApiClient {
    private val client = OkHttpClient()

    fun performGetRequest(
        altitude: Double,
        longitude: Double,
        radius: Int,
        tag: String
    ): GeoNodeResponseModel {
        val request = Request.Builder()
            .url("https://lz4.overpass-api.de/api/interpreter?data=[out:json];node[$tag](around:$radius,$altitude,$longitude);out;")
            .build()

        val mapper = jacksonObjectMapper()

        client.newCall(request).execute().use { response ->
            if (!response.isSuccessful) throw IOException("Unexpected code $response")
            return mapper.readValue(response.body!!.string(), GeoNodeResponseModel::class.java)
        }
    }
}