package org.example

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ElementResponseModelTest {
    @Test
    fun deserializeElementResponseModel() {
        val json = """{
                          "type": "node",
                          "id": 6894643099,
                          "lat": 1.2815196,
                          "lon": 103.8437522,
                          "tags": {
                            "amenity": "post_box",
                            "name": "Post box"
                          }
                        }"""
        val mapper = jacksonObjectMapper()
        val elementResponseModel: ElementResponseModel = mapper.readValue(json, ElementResponseModel::class.java)

        assertEquals(elementResponseModel.type, "node")
        assertEquals(elementResponseModel.id, 6894643099)
        assertEquals(elementResponseModel.lat, 1.2815196)
        assertEquals(elementResponseModel.lon, 103.8437522)
        val tags = mapOf("amenity" to "post_box", "name" to "Post box")
        assertEquals(elementResponseModel.tags, tags)
    }
}