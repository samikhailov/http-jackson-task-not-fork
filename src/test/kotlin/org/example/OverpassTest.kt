package org.example

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.greaterThanOrEqualTo
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class OverpassTest {
    @Test
    fun base() {
        val overpassApiClient = OverpassApiClient
        val result = overpassApiClient.performGetRequest(1.2815196, 103.8437522, 10, "amenity=post_box")
        assertEquals(result.version, 0.6)  // Проверка не очень, для текущего задания должна подойти
        assertEquals(
            result.generator,
            "Overpass API 0.7.56.9 76e5016d"
        )  // Проверка не очень, для текущего задания должна подойти
        val osm3s: Map<String, String> = mapOf(
            "copyright" to "The data included in this document is from www.openstreetmap.org. The data is made available under ODbL."
        )
        assertEquals(result.osm3s["copyright"], osm3s["copyright"])
        val tags = mapOf<String, Any>(
            "amenity" to "post_box",
            "name" to "Post box"
        )
        val elementResponse = listOf(
            ElementResponseModel(
                type = "node",
                id = 6894643099,
                lat = 1.2815196,
                lon = 103.8437522,
                tags = tags
            )
        )
        assertEquals(result.elements, elementResponse)
    }

    @ParameterizedTest(name = "stationSubway: {0}")
    @ValueSource(strings = ["59.954039,30.4129438", "59.9431492,30.2772645", "59.9390267,30.3442583"])
    fun stationSubway(coordinates: String) {
        val coordinatesArray = coordinates.split(",")
        val overpassApiClient = OverpassApiClient
        val result = overpassApiClient.performGetRequest(
            coordinatesArray[0].toDouble(),
            coordinatesArray[1].toDouble(),
            400,
            "station=subway"
        )
        assertThat(result.elements.size, greaterThanOrEqualTo(1))
    }

    @ParameterizedTest(name = "amenityBar: {0}")
    @ValueSource(strings = ["59.954039,30.4129438", "59.9431492,30.2772645", "59.9390267,30.3442583"])
    fun amenityBar(coordinates: String) {
        val coordinatesArray = coordinates.split(",")
        val overpassApiClient = OverpassApiClient
        val result = overpassApiClient.performGetRequest(
            coordinatesArray[0].toDouble(),
            coordinatesArray[1].toDouble(),
            400,
            "amenity=bar"
        )
        assertThat(result.elements.size, greaterThanOrEqualTo(2))
    }

}